/**
 * A BLE client example that is rich in capabilities.
 * There is a lot new capabilities implemented.
 * author unknown
 * updated by chegewara
 */

#include "BLEDevice.h"
//#include "BLEScan.h"

// The remote service we wish to connect to.
static BLEUUID serviceUUID("4fafc201-1fb5-459e-8fcc-c5c9c331914b");
// The characteristic of the remote service we are interested in.
static BLEUUID    charUUID("beb5483e-36e1-4688-b7f5-ea07361b26a8");

static double rssi;
static double txPower = -69.00;
static double txPower_rssi;
static double result;

static BLERemoteCharacteristic* pRemoteCharacteristic;
static BLEAdvertisedDevice* myDevice;

static void notifyCallback(
  BLERemoteCharacteristic* pBLERemoteCharacteristic,
  uint8_t* pData,
  size_t length,
  bool isNotify) {
    Serial.print("Notify callback for characteristic ");
    Serial.print(pBLERemoteCharacteristic->getUUID().toString().c_str());
    Serial.print(" of data length ");
    Serial.println(length);
    Serial.print("data: ");
    Serial.println((char*)pData);
}

/**
 * Scan for BLE servers and find the first one that advertises the service we are looking for.
 */
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
 /**
   * Called for each advertising BLE server.
   */
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    Serial.println(advertisedDevice.toString().c_str());
    String esp_name = advertisedDevice.getName().c_str();
    String esp_rssi = String(advertisedDevice.getRSSI());
    String esp_txpower = String(advertisedDevice.getTXPower());

    String findMe = "Find Me";
    String esp32_word = "MyESP32_Word";
    String esp32_a = "MyESP32_A";
    String esp32_b = "MyESP32_B";
    String esp32_c = "MyESP32_C";
   
    if(esp_name == esp32_word || esp_name == esp32_a || esp_name == esp32_b || esp_name == esp32_c) {
      Serial.println("|-----------------------------------|");
      Serial.println("|                                   |");
      Serial.println("|        ESP32 NAME: " + esp_name + "        |");
      Serial.println("|        ESP32 RSSI: " + esp_rssi + "            |");
      Serial.println("|        ESP32 TxPower: " + esp_txpower + "           |");
      Serial.println("|                                   |");
      Serial.println("|-----------------------------------|");
      Serial.println();
      Serial.println();
      rssi = advertisedDevice.getRSSI();
      txPower_rssi = (txPower - (rssi)) / 20.0;
      result = pow(10, txPower_rssi);
      Serial.print("range: ");
      Serial.println(result);
    } else if (esp_name == esp32_word && esp_name == esp32_a && esp_name == esp32_b && esp_name == esp32_c) {
         BLEDevice::getScan()->stop();
    }
  } // onResult
}; // MyAdvertisedDeviceCallbacks


void setup() {
  Serial.begin(115200);
  Serial.println("Starting Arduino BLE Client application...");
  BLEDevice::init("");

  // Retrieve a Scanner and set the callback we want to use to be informed when we
  // have detected a new device.  Specify that we want active scanning and start the
  // scan to run for 5 seconds.
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setInterval(1349);
  pBLEScan->setWindow(449);
  pBLEScan->setActiveScan(true);
  pBLEScan->start(5, false);
} // End of setup.


// This is the Arduino main loop function.
void loop() {
    BLEDevice::getScan()->start(0);  // this is just eample to start scan after disconnect, most likely there is better way to do it in arduino
  
  delay(1000); // Delay a second between loops.
} // End of loop
